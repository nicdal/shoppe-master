class AddExtraInfoToShoppeOrderItems < ActiveRecord::Migration
  def change
    add_column :shoppe_order_items, :extra_info, :string, limit: 1000
  end
end
